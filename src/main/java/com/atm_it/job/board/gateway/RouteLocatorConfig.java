package com.atm_it.job.board.gateway;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.stereotype.Component;

/**
 * Created by ATAF MOHELLEBI on 24/11/2020.
 */

@Component
public class RouteLocatorConfig {

    private static final String LOAD_BALANCE_PREFIX = "lb://";
    private static final String MS_COMPANY          = "MS-JBB-COMPANY";
    private static final String MS_CANDIDATE        = "MS-JBB-CANDIDATE";

    public RouteLocator buildRoutes(final RouteLocatorBuilder.Builder builder) {

        builder
          .route(r -> r.path("/company/**")
                       .uri(LOAD_BALANCE_PREFIX.concat(MS_COMPANY))
                       .id(MS_COMPANY))
          .route(r -> r.path("/candidate/**")
                       .uri(LOAD_BALANCE_PREFIX.concat(MS_CANDIDATE))
                       .id(MS_CANDIDATE));

        return builder.build();
    }

}
